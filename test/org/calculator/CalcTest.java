package org.calculator;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalcTest {

	@Test
	public void testMultiply() {
		for(int i = 0; i < 1_000_000; i++) {
			int a = (int)(Math.random() * 100);
			int b = (int)(Math.random() * 100);
			
			assertEquals(a * b, Calc.multiply(a, b));
		}
	}
}
